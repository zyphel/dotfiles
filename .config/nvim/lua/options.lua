-- options

vim.wo.number = true
vim.o.relativenumber = false

vim.o.autoread = true
vim.o.autowrite = true
vim.o.backspace = '2'
vim.o.backup = false
vim.o.clipboard = "unnamedplus"
vim.o.confirm = true -- prompt to save before destructive actions
vim.o.display = 'lastline'
vim.o.history = 1000

vim.o.hlsearch = false
vim.o.ignorecase = true

vim.opt.termguicolors = true

vim.o.incsearch = true
vim.o.laststatus = 2
vim.o.linebreak = true -- wrap, but on words, not randomly
vim.o.mouse = 'a'
-- vim.o.scrolloff = 18 -- number of lines above and below cursor
vim.o.showcmd = true
vim.o.smartcase = true
vim.o.swapfile = false
vim.o.undofile = true -- save undo history
vim.o.updatetime = 50 -- fast update time
vim.o.wrap = true
vim.o.wildmenu = true
vim.o.writebackup = false

-- spaces for tabs etc
vim.opt.expandtab = true -- use spaces instead of tabs
vim.opt.softtabstop = 2 -- number of spaces tabs count for
vim.opt.shiftwidth = 2 -- indent size
vim.opt.shiftround = true -- round indent
vim.o.joinspaces = false -- No double spaces with join after a dot

-- spell check
vim.o.spelllang = 'en_us'
vim.o.spell = true
-- vim.cmd[[autocmd FileType markdown,typ setlocal spell]]

-- autosave/update upon insert leave
vim.api.nvim_exec([[ autocmd InsertLeave * update ]], false)

-- folds

vim.cmd([[
" options 
set viewoptions=folds,cursor
set sessionoptions=folds

" persistent folds
augroup AutoSaveGroup
  autocmd!
	" view files are about 500 bytes
	" bufleave but not bufwinleave captures closing 2nd tab
	" nested is needed by bufwrite* (if triggered via other autocmd)
	" BufHidden for compatibility with `set hidden`
  autocmd BufWinLeave,BufLeave,BufWritePost,BufHidden,QuitPre ?* nested silent! mkview!
  autocmd BufWinEnter ?* silent! loadview
augroup end
]])
