-- gruvbox options

vim.g.gruvbox_terminal_colors = true
vim.g.gruvbox_transparent = true

-- vim.cmd[[hi NvimTreeNormal guibg=NONE ctermbg=NONE]]

vim.cmd[[colorscheme gruvbox-flat]]
-- vim.cmd ('colorscheme gruvbox-flat')
