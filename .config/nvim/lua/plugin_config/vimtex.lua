vim.cmd([[
let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex' 
let g:vimtex_view_method = 'zathura'
autocmd FileType tex :NoMatchParen
au FileType tex setlocal nocursorline
]])
