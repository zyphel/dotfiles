-- vim-pencil options

vim.g.tex_conceal = ""
vim.g['pencil#conceallevel'] = 0
vim.g['pencil#wrapModeDefault'] = 'soft'

-- vim.api.nvim_exec([[ autocommand goes here ]], false)

vim.api.nvim_exec([[ autocmd FileType markdown,mkd call pencil#init() ]], false)
vim.api.nvim_exec([[   autocmd FileType text call pencil#init() ]], false)
