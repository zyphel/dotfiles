-- nvim-tree options

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- invoke nvim-tree
vim.keymap.set('n', '<c-n>', ':NvimTreeFindFileToggle<CR>')

require('nvim-tree').setup({
  view = { width = 30 },
  auto_reload_on_write = true,
  actions = { use_system_clipboard = true, },
})
