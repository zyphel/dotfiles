require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'gruvbox-flat',
  },
  sections = {
    lualine_a = {
      {
        'filename',
        path = 1,
      }
    }
  }
}
