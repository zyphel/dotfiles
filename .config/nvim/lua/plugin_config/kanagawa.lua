vim.g.gruvbox_terminal_colors = true
vim.g.gruvbox_transparent = true

vim.cmd('colorscheme kanagawa-dragon')
