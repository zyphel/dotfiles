-- keymaps

vim.g.mapleader = ' '
vim.g.maplocalleader = ' '


vim.keymap.set('n', '<leader>zz', vim.cmd.ZenMode)
vim.keymap.set('n', '<leader>ee', vim.cmd.Ex)
vim.cmd [[nnoremap <leader>cd :cd %:p:h<CR>:pwd<CR>]]

-- vim.keymap.set('n', '<leader>pv', vim.cmd.Ex)

-- vim.keymap.set('n', '<leader>h', ':nohlsearch<CR>')
